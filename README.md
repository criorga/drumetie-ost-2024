# Drumetie OST 2024
Locatie: Cheile Rametului
Cazare: Cabana Ramet
Costuri: https://docs.google.com/document/d/1nCXNCYpktASQwa2tlipc06bR6BTe_uILnT2Z5j3rCrU/edit
Checklist necesare: https://docs.google.com/document/d/13TON4YI2vZ88CYP6xvf38l5gGFo76bGq-q8KpezHwig/edit


Idei activitati:<br /> 
impartit pe echipe de "cercetasi", fiecare echipa completeaza task-uri si castiga puncte.<br /> 
4 echipe de 5 oameni: ursuletii, lupisorii, porcusorii mistreti si caprioarele<br /> 
<br /> 
Premii: <br /> 
La inceputul activitatii:<br /> 
- insigne pentru fiecare persoana cu simbolul echipei sale
<br /> _Justificare: asocierea mai puternica la un nivel personal a fiecarui voluntar cu proiectul, apartenenta la un grup, promovarea implicarii voluntarilor si a unitatii grupului_
<br /> La finalul activitatii (ceremonie de premiere la foc de tabara)
- insigne personale legate de performanta echipei (locul 1, 2 si 3)
<br /> _Justificare: premierea performantei, promovarea implicarii si a unui spirit constructiv de competitivitate_
- 3 insigne pentru: cel mai disciplinat cercetas, cel mai prietenos cercetas si cel mai implicat cercetas
_Justificare: premierea si promovarea disciplinei, atitudinii prietenoase si a implicarii in activitati, aducerea oportunitatii de a se diferentia pe plan personal separat de echipa si de competitie_
<br /> 
<br /> Activitati: 
<br /> Modelul cercetasului OST-ist:
<br /> "Un cercetas OST-ist trebuie sa fie stilat, cult, agil, puternic, un bun culegator, un bun vanator, un bun navigator si nu in cele din urma un iubitor si un protector aprig al naturii,semenilor si tarii lui."
<br /> Activitati de cunoastere:
<br /> _Justificare: Activitatile de cunoastere faciliteaza socializarea dintre participanti, usurand crearea de legaturi si prietenii intre acestia._
- 2 adevaruri si o minicuna - fiecare spune 2 adevaruri si o minciuna, restul grupului voteaza care e minicuna
- Daca as fi fost.. - fiecare spune" Daca as fi fost un animal, as fi fost _ pentru ca _"
- Cercul de povesti - fiecare povesteste pe scurt despre un topic din viata sa (cea mai aventuroasa noapte, cel mai rusinos moment)

<br /> Activitati de competitie intre echipe prin intermediul jocului.
<br /> _Justificare: astfel de activitati ajuta voluntarii sa invete si sa lege legaturi de prietenie prin intermediul jocurilor, stimuleaza in mod constructiv competitivitatea voluntarilor si dorinta voluntarilor de invatare si autodezvoltare._

<br /> Pornind de la modelul cercetasului OST-ist, sustragem urmatoarele categorii de activitate:
<br /> Stil, Cultura, Vanatoare & Cules, Agilitate, Forta, Navigare
<br /> _Fiecare categorie are mai multe activitati de unde putem alege. Nu e necesar sa fie facute toate in decursul unui singur proiect si nici ca toate categoriile sa aiba activitati desfasurate._
<br /> **Stil**
- challenge poze (cu o ciuperca, cu un conifer, langa apa, etc)
- concurs de moda (fiecare echipa alege un reprezentant, coordonatorii dau nota)

**Cultura** 
- Trivia despre geografia locatiei/ altor locatii din Romania
- Trivia despre de supravietuire si orientare
_putem folosi kahoot_

**Vanatoare & Cules**
- Tras cu arcul cu sageti ventuza la tinta
- Foraging hunt pentru obiecte ascunse (eventual in atingerea unui scop- sa faca focul, sa construiasca casute pentru buburuze etc)
- Construit de mini-capcane

**Agilitate**
- Intreceri

**Forta**
- Tug of war (tras de sfoara)

**Navigare**
- Gasiti comoara cu harta si busola

